# LiveValidator - Documentation

[![build status](https://gitlab.com/LiveValidator/livevalidator.gitlab.io/badges/master/build.svg)](https://gitlab.com/LiveValidator/livevalidator.gitlab.io/commits/master)

This contains the full documentation for LiveValidator.

Find the project [home page and docs](https://livevalidator.gitlab.io/).
