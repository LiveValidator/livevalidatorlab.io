---
date: 2016-07-20T16:01:23+02:00
description: These are the basic steps to get started with the plug-in
menu:
  main:
    weight: 2
title: Getting Started
type: doc
---
## Creation
This plug-in is written in vanilla JS and can be call on `HTMLElement`s, `HTMLCollection`s and `NodeList`s. All of these will have the `getLiveValidator` method on them.

If you are using the [jQuery plug-in](https://gitlab.com/LiveValidator/Plugin-JQuery) then it is also possible to call `LiveValidator` on a jQuery object.

Both of these will filter the element(s), that it is called on, to only contain inputs that the validator can work on. Children of the respective element(s) will also be considered. All valid inputs will have an instance of LiveValidator's Core instatiated on them. Then a **plug-in object** will be returned that you can use to interact with the inputs as a whole group via [methods]({{<ref "methods.md" >}}).

### Calling the plug-in
{{% example "options-required" %}}
{{% alert %}}Notice how the required attribute is detected on the input and handled by the theme to add the asterisk{{% /alert %}}

### Passing options to the plug-in
It is also possible to overwrite the plug-in options per instantiation. Just pass in a object with any of the valid [options]({{< ref "options.md" >}}) to the method.
{{% example "gs-options" %}}

### Passing options via data
The plug-in will also check for `data-*` attributes and combines them into the options. This makes it easy to have a lot of inputs with many different options.
{{% example "gs-data-options" %}}
{{% alert %}}Be sure to set array & object options correctly. This is one example of how it can be done - with the double quotes on the inside{{% /alert %}}

### Changing default options
Sometimes it is also needed to change the default options before initiating a set of inputs - setting the [theme]({{<ref "options.md#theme">}}) and [theme data]({{<ref "options.md#theme-data">}}) is a good example of this.

To do this, call `LiveValidator.Plugin` or for jQuery just call the plug-in directly with `$.LiveValidator` and pass in a object of [options]({{< ref "options.md" >}}) to overwrite.
{{% example "gs-default-options" %}}

{{% alert %}}Note that required will be true if either the default or options passed has required as true{{% /alert %}}
{{% alert %}}Also note how to instantiate multiple inputs with one call - which can be an alternative to overwriting the defaults. This is a result of the filter process{{% /alert %}}

## After creation
Now that you know how to create an instance of the plug-in on inputs, you will want to call some important [methods]({{<ref "methods.md" >}}) on the returned **plug-in object**. The most important of these will be the [`isValid` method]({{<ref "methods.md#isvalid" >}}).
