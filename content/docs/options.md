---
date: 2016-07-20T15:59:02+02:00
description: Here are all the options that can be set on instantiation or via defaults overwrite
menu:
  main:
    weight: 3
title: Options
type: doc
---
## Checks
{{% muted %}}Default: []{{% /muted %}}
This is an array that holds the checks for the input(s). It is important to use valid object syntax when it is set via the `data-checks` attribute.

Any of the [precreated checks]({{<ref "testers.md">}}) are valid or [custom ones]({{<ref "testers.md#create-own-testers">}}) that you created yourself.

### Checks with parameters
Some checks need to be called with extra parameters. These can be passed to the check by setting the check to an object with the key as the check's name and the value will be the parameter.

#### Parameters example
{{% example "options-check-parameters" %}}
{{% alert %}}Note how the value can be a string, int or object{{% /alert %}}

## Live Enabled
{{% muted %}}Default: true{{% /muted %}}
`liveEnabled` is used to set whether checking should happen live as the user types. Setting it to false results in checking only happening once the input looses focus.

### Disabling live results
{{% example "options-live-enabled" %}}

## Required
{{% muted %}}Default: false{{% /muted %}}
Used to indicate if the input is required. It is automatically set when the input has the `required` property.

### Setting required via property detection
{{% example "options-required" %}}
{{% alert %}}Note that the theme automatically handles the asterisk{{% /alert %}}

## Locale
{{% muted %}}Default: 'en-us'{{% /muted %}}
This sets the locale used in [translation]({{<ref "translations.md">}}) for errors that are identified.

### Available locales
Currently the following are available and you can [add your own]({{<ref "translations.md#translations-for-custom-testers">}}) for any check (default or manual). You can also [overwrite]({{<ref "translations.md#translate-default-errors">}}) them as needed.

| Code                                                    | Language                                         |
|:-------------------------------------------------------:|:-------------------------------------------------|
|  [af](https://gitlab.com/LiveValidator/Translation-af)  | Afrikaans                                        |
| en-us                                                   | English (US) - default on all precreated testers |


## Theme
{{% muted %}}Default: undefined{{% /muted %}}
Set the theme object that you want to use here - see [themes]({{<ref "themes.md">}}) to get the correct name. If no theme is set or if the theme is invalid, then instantiation will fail.

### Theme Data
{{% muted %}}Default: {}{{% /muted %}}
The data, array or object that is set here will be passed to the theme as is. Consult the [theme]({{<ref "themes.md">}}) you are using to know how to overwrite some options.

## Debug
{{% muted %}}Default: false{{% /muted %}}
This option can be used to log debug information to the console. These can be helpful to find out why something is not working - like in a bug report.

### Options

Option    | Description
:--------:|:------------------------------------------
1 \| true | Show only 'DEBUG' data
2         | Also show 'INFO' data
3         | Also show 'ERROR' data
