---
date: 2016-11-29T16:28:51+02:00
description: The heart of any validation is the testers that you want to run to validate the input
menu:
  main:
    weight: 4
title: Testers
type: doc
---

## HTML5
These tests are currently the only precreated ones available. They are mainly to fill the gap of their HTML5 counterparts. Also included is the key that each use from the translation file for its error - if it uses one.
They are all in `livevalidator-tester-html5` on NPM or Bower.

### min
{{% muted %}}Parameter: int; Error Key: minNumber{{% /muted %}}
The minimum number that an input needs to be considered valid.

### max
{{% muted %}}Parameter: int; Error Key: maxNumber{{% /muted %}}
This makes sure that the input does not have a number that is bigger than the supplied number.

#### Min and Max example
{{% example "tester-min-max" %}}
{{% alert %}}Note that both of these use [`isNumber`]({{<ref "#isnumber">}}) to make sure the input is a number{{% /alert %}}

### minlength
{{% muted %}}Parameter: int; Error Key: minlength{{% /muted %}}
Used to control the minimum number of characters that the input must have.

### maxlength
{{% muted %}}Parameter: int; Error Key: maxlength{{% /muted %}}
Controls the maximum number of characters allowed in the input.

#### Minlength and Maxlength example
{{% example "tester-minlength-maxlength" %}}

### pattern
{{% muted %}}Parameter: {pattern: 'regex', title: 'Error message'}{{% /muted %}}
A regex pattern that the input has to match. Include the helper text in the title as it will be used if the pattern fails.

#### Pattern example (Auto detected)
{{% example "tester-pattern" %}}
{{% alert %}}The pattern tester is auto detected by its attribute because `livevalidator-tester-html5` comes with its own options parser for detecting checks based on HTML5 attributes - which is true for all of the above too{{% /alert %}}

### email
{{% muted %}}Parameter: boolean; Error Key: email{{% /muted %}}
Checks that the input is a valid email. The passed in boolean sets whether multiple emails are allowed - separated with commas - and is `false` by default.

#### Multiple email example (Auto detected)
{{% example "tester-email" %}}
{{% alert %}}The email tester is auto detected by its type, and set to multiple since it has the `multiple` property, because of the HTML5 optionsParser{{% /alert %}}

### isNumber
{{% muted %}}Parameter: None; Error Key: beNumber{{% /muted %}}
Not an official HTML5 validator, but is used by [`min`]({{<ref "#min" >}}) and [`max`]({{<ref "#max" >}}) to make sure the input is a number.

## Create Own Testers
The modular design of LiveValidator allows you create your own testers to suit your specific needs.

### Basic Testers
The basic is to add a function to `LiveValidator.Tester.prototype`. The key of the attribute will act as the tester's name, while the value and any set parameters will be passed to this function. The use of the `addError` method allows you set error message as certain test condition fails.
{{% example "tester-own-basic" %}}

### Chaining Testers
Above you may have notices that the tester returns a boolean status. This is optional, but the use of it allows you to chain your testers to make complex conditions easier. You can do this by calling another tester from within a tester as a method, of `this`, with the name being the tester's name.
{{% example "tester-own-chain" %}}
{{% alert %}}This is almost the same as having multiple checkers. But the difference is that the parent test has to pass first before the child is checked with chaining, while both will run if multiple checkers were used. A good use of chaining is with `min` and `max` above where it will only make sense to check the number range after it is a valid number{{% /alert %}}

### Using Translation Messages
If you are creating an international application, then you probably do not want to hard code the error messages. This will allow you to define a test once and just swap the translation file and locale to get translated errors. The use of the `getMessage` method allows you to get the set [translation message]({{<ref "translations.md">}}). The value passed to this method is the `key` of the message.
{{% example "tester-own-translation" %}}
