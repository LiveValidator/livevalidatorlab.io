---
date: 2016-11-30T10:42:59+02:00
description: Because every design is different you will need the right theme for your design
menu:
  main:
    weight: 6
title: Themes
type: doc
---
## Available Themes
It is because of this reason that LiveValidator has a [default theme]({{<ref "default.md">}}), a few for use with popular CSS framework and then it is also possible to create your own theme.

Popular framework themes include

### Bootstrap 3
There are three Bootstrap 3 themes. One [simple theme]({{<ref "bootstrap3.md">}}) that uses the `help-block` of the form element, another that uses the [tooltip]({{<ref "bootstrap3tooltip.md">}}) and yet another that uses the [popover]({{<ref "bootstrap3popover.md">}}).

### UIkit2
For UIkit2 there is only two. The first, [basic theme]({{<ref "uikit2.md">}}) uses `uk-form-help-block` with `uk-text-danger` for error's styling. The other option is the [UIkit2 tooltip theme]({{<ref "uikit2tooltip.md">}}).

## Create Own Theme
You can also create your own theme. To do this you have two options... well maybe three.

### Use the Default Theme
You can use the default (or any of the other themes) if their structure closely produces what you want. Then just change the [themeData]({{<ref "options.md#theme-data" >}}) to suite your needs and use your own CSS. This will be the tricky, but sometimes simple, third options.

### Inherit from Default Theme
Then you can also create a new JS object that inherits from the default theme. This way you know that your theme is complete (that it has all the needed methods) and you can just overwrite the methods that you want changed. The down side is that you will have to ship the theme with the default theme's code.

### Create JS Theme from Scratch
Lastly, if you want, you can create a JS object that you also pass in as the [theme]({{<ref "options.md#theme">}}) option. This object will need the following methods.

- A **constructor** that will receive the input element and the themeData
- **markRequired** which will be called when the input is required or set so - consider the case where the input may already have its required marking.
- **unmarkRequired** that will remove the mark from a required field
- **setMissing** which should mark the field as missing input when it is empty and required
- **unsetMissing** that should restore the field once its missing status is corrected
- **addErrors** which will receive an array of errors to guide the user to correct the input errors
- **clearErrors** to remove all the errors from the field again

On instantiation the core will look for these methods and will reject the theme if they do not exist.

## Helpful Utils
On the `LiveValidator.utils` object you will find the following functions to aid you in your theme creation if you want to keep the theme vanilla JS.

### extend
{{% muted %}}Parameters: List of objects to extend unto first{{% /muted %}}
{{% muted %}}Returns: The extended object{{% /muted %}}
This is like jQuery's extend, except that it will always deep extend unto the first object given.

### getData
{{% muted %}}Parameters: HTMLElement{{% /muted %}}
{{% muted %}}Returns: Object{{% /muted %}}
You may never need this really, but it will get all `data-*` on an element and will also handle JSON data values.

### parentSelector
{{% muted %}}Parameters: HTMLElement, selectorString{{% /muted %}}
{{% muted %}}Returns: HTMLElement or null if not found{{% /muted %}}
This will find the parent, of the passed in element, that matches the selector string - which can be any valid CSS selector.

### addClass
{{% muted %}}Parameters: HTMLElement, class{{% /muted %}}
{{% muted %}}Returns: true if successful{{% /muted %}}
This will add a single class to an element based on browser support.

### removeClass
{{% muted %}}Parameters: HTMLElement, class{{% /muted %}}
{{% muted %}}Returns: true if successful{{% /muted %}}
This will remove a single class from an element based on browser support.

### removeChild
{{% muted %}}Parameters: HTMLElement, childSelector{{% /muted %}}
{{% muted %}}Returns: The removed HTMLElement, else null{{% /muted %}}
This will remove a child element if the passed in element is valid and if the child element can be found.

### appendChild
{{% muted %}}Parameters: HTMLElement, HTMLElement{{% /muted %}}
{{% muted %}}Returns: The appended HTMLElement, else null{{% /muted %}}
This will append the second HTMLElement unto the first only if the first is a valid HTMLElement.

### matches
{{% muted %}}Parameters: CSS selector{{% /muted %}}
{{% muted %}}Returns: True if matches{{% /muted %}}
This is not part of the utils, but is the real [matches method](https://developer.mozilla.org/en/docs/Web/API/Element/matches) with the [polyfill](https://developer.mozilla.org/en/docs/Web/API/Element/matches#Polyfill) added for browsers that to not support it yet.
