---
date: 2016-11-30T10:52:24+02:00
description: Bootstrap 3 theme for LiveValidator
menu:
  main:
    parent: 'Themes'
    weight: 1
title: Bootstrap 3
type: theme
theme: Bootstrap3
preview:
  css: Bootstrap3
options:
    error:
        description: Class to apply to parent element when there is an error with the input
        default: has-warning
    missing:
        description: Class to apply to parent element when the input is missing and required
        default: has-error
    parentSelector:
        description: The selector used to find the parent element to apply the classes to
        default: .form-group
form: |
    <form novalidate>
        <div class="form-group">
            <label for="Name">Name</label>
            <input type="text" class="form-control" id="Name" placeholder="Name" data-checks='[ {"minlength": 3}, {"maxlength": 25} ]' required>
        </div>
        <div class="form-group">
            <label for="age">Age</label>
            <input type="text" class="form-control" id="age" placeholder="Age" data-checks='[ {"min": 10}, {"max": 100} ]' required>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
---
This is the basic Bootstrap 3 theme that does not need jQuery or any other Bootstrap JS components. It uses the `help-block` to display the errors to the users
