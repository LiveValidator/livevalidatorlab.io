---
date: 2016-11-30T10:52:24+02:00
description: Bootstrap 3 Popover theme for LiveValidator
menu:
  main:
    parent: 'Themes'
    weight: 1
title: Bootstrap 3 Popover
type: theme
theme: Bootstrap3Popover
preview:
  css: Bootstrap3
  js: Bootstrap3
options:
    error:
        description: Class to apply to parent element when there is an error with the input
        default: has-warning
    missing:
        description: Class to apply to parent element when the input is missing and required
        default: has-error
    parentSelector:
        description: The selector used to find the parent element
        default: .form-group
    popover:
        description: The options that will be passed to the popover on instatiation
        default: "{ placement: 'bottom', trigger: 'focus', title: 'Keep these in mind' }"
form: |
    <form novalidate>
        <div class="form-group">
            <label for="Name">Name</label>
            <input type="text" class="form-control" id="Name" placeholder="Name" data-checks='[ {"minlength": 3}, {"maxlength": 25} ]' required>
        </div>
        <div class="form-group">
            <label for="age">Age</label>
            <input type="text" class="form-control" id="age" placeholder="Age" data-checks='[ {"min": 10}, {"max": 100} ]' required>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
---
This Bootstrap 3 theme uses the popover by Bootstrap to display the errors. It therefore needs jQuery and the Bootstrap JS code.
