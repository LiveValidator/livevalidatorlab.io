---
date: 2016-11-30T10:52:24+02:00
description: Default theme for LiveValidator
menu:
  main:
    parent: 'Themes'
    weight: 0
title: Default
type: theme
theme: Default
options:
    error:
        description: Class to apply to parent element when there is an error with the input
        default: error
    missing:
        description: Class to apply to parent element when the input is missing and required
        default: missing
    parentSelector:
        description: The selector used to find the parent element to apply the classes to
        default: .row
form: |
    <form novalidate>
        <div class="row">
            <label for="Name">Name</label>
            <input type="text" id="Name" placeholder="Name" data-checks='[ {"minlength": 3}, {"maxlength": 25} ]' required>
        </div>
        <div class="row">
            <label for="age">Age</label>
            <input type="text" id="age" placeholder="Age" data-checks='[ {"min": 10}, {"max": 100} ]' required>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
---
This is a vanilla JS theme (which tries to be simple) and you will need to also include its CSS. It only provides styling for the inputs and their errors and not any buttons or check/radio boxes.
