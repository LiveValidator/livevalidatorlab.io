---
date: 2016-11-30T10:52:24+02:00
description: UIkit2 theme for LiveValidator
menu:
  main:
    parent: 'Themes'
    weight: 1
title: UIkit2
type: theme
theme: UIkit2
options:
    danger:
        description: The class to apply to the input when it is missing or has an error
        default: uk-form-danger
    success:
        description: This class gets applied to the input when it has no errors
        default: uk-form-success
    parentSelector:
        description: This is used to find the row of the input to be able to find the controls and label
        default: .uk-form-row
    controlsSelector:
        description: The selector to find the controls from the parent
        default: .uk-form-controls
form: |
    <form novalidate class="uk-form uk-form-stacked">
        <div class="uk-form-row">
            <label for="Name" class="uk-form-label">Name</label>
            <div class="uk-form-controls">
                <input type="text" id="Name" class="uk-width-1-1" placeholder="Name" data-checks='[ {"minlength": 3}, {"maxlength": 25} ]' required>
            </div>
        </div>
        <div class="uk-form-row">
            <label for="age" class="uk-form-label">Age</label>
            <div class="uk-form-controls">
                <input type="text" id="age" class="uk-width-1-1" placeholder="Age" data-checks='[ {"min": 10}, {"max": 100} ]' required>
            </div>
        </div>
        <div class="uk-form-row">
            <button type="submit" class="uk-button">Submit</button>
        </div>
    </form>
---
This is the basic UIkit2 theme that does not need jQuery or any other UIkit2 JS components. It uses the `uk-form-help-block` together with `uk-text-danger` to display the errors to the users
