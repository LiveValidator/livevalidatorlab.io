---
date: 2016-11-30T10:52:24+02:00
description: UIkit2 Tooltip theme for LiveValidator
menu:
  main:
    parent: 'Themes'
    weight: 1
title: UIkit2 Tooltip
type: theme
theme: UIkit2Tooltip
preview:
  css: UIkit2
  js: UIkit2
options:
    danger:
        description: The class to apply to the input when it is missing or has an error
        default: uk-form-danger
    success:
        description: This class gets applied to the input when it has no errors
        default: uk-form-success
    parentSelector:
        description: This is used to find the row of the input to be able to find the controls and label
        default: .uk-form-row
    controlsSelector:
        description: The selector to find the controls from the parent
        default: .uk-form-controls
    tooltip:
        description: The data that gets passed to the tooltip on instatiation
        default: "{ pos: 'bottom-left', animation: true }"
form: |
    <form novalidate class="uk-form uk-form-stacked">
        <div class="uk-form-row">
            <label for="Name" class="uk-form-label">Name</label>
            <div class="uk-form-controls">
                <input type="text" id="Name" class="uk-width-1-1" placeholder="Name" data-checks='[ {"minlength": 3}, {"maxlength": 25} ]' required>
            </div>
        </div>
        <div class="uk-form-row">
            <label for="age" class="uk-form-label">Age</label>
            <div class="uk-form-controls">
                <input type="text" id="age" class="uk-width-1-1" placeholder="Age" data-checks='[ {"min": 10}, {"max": 100} ]' required>
            </div>
        </div>
        <div class="uk-form-row">
            <button type="submit" class="uk-button">Submit</button>
        </div>
    </form>
---
This UIkit2 theme uses UIkit2's tooltip component, therfore be sure to include jQuery and UIkit2's JS as well as the tooltip's CSS and JS
