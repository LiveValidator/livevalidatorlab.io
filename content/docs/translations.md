---
date: 2016-11-30T10:03:52+02:00
description: All error translations are simply an object
menu:
  main:
    weight: 5
title: Translations
type: doc
---

### Translate default errors
The errors for the default checks can be translated by just adding them to `LiveValidator.translations.locale.key`. Where `locale` is the locale code and `key` is the string used to access and identify the message.

The same can be used to overwrite a message if you know its error key.

#### Translation example
{{% example "translation-af" %}}
{{% alert %}}Notice how the above falls back to the 'en-us' message for the missing `beNumber` message. This is the default, so it would be optimal if the 'en-us' locale is always complete for any messages, that you may have for custom testers, to allow the fallback to work{{% /alert %}}

### Translations for custom testers
You can add translation message for use in [custom tester]({{<ref "testers.md#create-own-testers">}}) in the same way by using the correct `locale` and `key` that you want to use to identify the message.

#### Custom tester with own translation example
{{% example "tester-own-translation" %}}
