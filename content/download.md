---
date: 2016-07-20T14:26:22+02:00
description: Use these instructions to get Live Validator for your project
menu:
    main:
        weight: 1
title: Download
type: doc
---

## Zip Download
To get the latest distribution version as a ZIP use the following button. Since the entire code is decoupled, it is a bit harder to get the source in one go. But the most important piece, so called the core, source can be gotten below too.

{{% button href="https://gitlab.com/LiveValidator/livevalidator.gitlab.io/builds/artifacts/%s/download?job=Distribute" type="primary" hrefParamSub="version" %}}Download{{% /button %}}
{{% button href="https://gitlab.com/LiveValidator/core/repository/archive.zip?ref=%s" type="success" hrefParamSub="version" %}}Source{{% /button %}}

### HTML Markup Example (Vanilla JS)
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Page Title</title>
        <!-- Include the theme style if needed -->
        <link rel="stylesheet" href="/css/LiveValidator/themes/default.min" charset="utf-8">
    </head>
    <body>
        <form action="#" method="post">
            <!-- ... -->
        </form>
        <!-- For vanilla JS -->
        <script src="js/LiveValidator/livevalidator.min.js" charset="utf-8"></script>
        <script src="js/LiveValidator/themes/default.min.js" charset="utf-8"></script>
        <!-- Include a translation if you need one, or base your own on a subset of one -->
        <script src="js/LiveValidator/translations/en-us.min.js" charset="utf-8"></script>
        <script type="text/javascript">
            var validationGroup = document.getElementsByTagName( 'form' ).getLiveValidator();
        </script>
    </body>
</html>
```

### HTML Markup Example (jQuery)
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Page Title</title>
        <!-- Include the theme style if needed -->
        <link rel="stylesheet" href="/css/LiveValidator/themes/default.min" charset="utf-8">
    </head>
    <body>
        <form action="#" method="post">
            <!-- ... -->
        </form>

        <!-- If you want to use jQuery -->
        <script src="js/jquery.min.js" charset="utf-8"></script>
        <script src="js/LiveValidator/livevalidator-jquery.min.js" charset="utf-8"></script>
        <script src="js/LiveValidator/themes/default.min.js" charset="utf-8"></script>
        <!-- Include a translation if you need one, or base your own on a subset of one -->
        <script src="js/LiveValidator/translations/en-us.min.js" charset="utf-8"></script>
        <script type="text/javascript">
            $( function() {
                var validationGroup = $( 'form' ).LiveValidator();
            } );
        </script>
    </body>
</html>
```
{{% alert %}}In both cases a **plug-in object** gets returned that you can use to interact with all the inputs{{% /alert %}}

## Bower
If you are using Bower to manage your assets, then you will find the plugin listed as `livevalidator`.

### Install
It can be installed using the following
```
bower install --save livevalidator
```
{{% alert %}}This will give you the [source file structure]({{<ref "#source-structure">}}) with `src` only{{% /alert %}}

## File structure
The zip file will contain either the distribution code that is ready to use as is. Or if you downloaded the source, you will have to make sure that you include the correct files for the plug-in to function properly.

### Distribution structure
Folder | Description
-------|----------------------------------------------------------------------
js     | Contains compiled version of the JavaScripts
css    | Contains the processed styles for the themes, if the theme needs extra styles

```markup
/js
    <!-- Contains the core, plug-in (and jQuery plugin if needed) and tester -->
    /livevalidator.js
    /livevalidator.min.js
    /livevalidator-jquery.js
    /livevalidator-jquery.min.js

    <!-- The JavaScript for all the available themes -->
    /themes
        /default.js
        /default.min.js
        /bootstrap3.js
        /bootstrap3.min.js
        /bootstrap3Popover.js
        /bootstrap3Popover.min.js
        /bootstrap3Tooltip.js
        /bootstrap3Tooltip.min.js
        /uikit2.js
        /uikit2.min.js
        /uikit2tooltip.js
        /uikit2tooltip.min.js

    <!-- Some precreated testers with default error messages for `en-us`
        (some might also have code to parse options) -->
    /testers
        /html5.js
        /html5.min.js

    <!-- The available extra translations for the error messages -->
    /translations
        /af.js
        /af.min.js

/css
    <!-- The styles for any themes that might need them -->
    /themes
        /default.css
        /default.min.css
```
{{% alert %}}The core, a theme and testers are needed at a minimum{{% /alert %}}

### Source structure
Folder | Description
-------|----------------------------------------------------------------------
js    | Contains all the source files
tests  | Contains all the test code

The structure of the `js` subfolder is a follow
```markup
/js
    <!-- The core that binds everything together -->
    core.js

    <!-- This creates the vanilla JS plugin -->
    plugin.js

    <!-- The skeleton for the tester that can be extended with your own tests -->
    tester.js

    <!-- Utils that the plugin and themes use to stay vanilla JS -->
    utils.js
```

#### Needed files
If you want to include the source files into your own as a bundle maybe, then you will need these at the minimum:

- core.js
- tester.js
- utils.js
- plugin.js (not strictly needed, but it makes handling multiple inputs easier)

{{% alert danger %}}Always include a theme too - instantiation will fail if one is not set.{{% /alert %}}
