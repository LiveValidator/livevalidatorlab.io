var gulp = require( 'gulp' ),
    jscs = require( 'gulp-jscs' ),
    jshint = require( 'gulp-jshint' ),
    stylish = require( 'gulp-jscs-stylish' ),
    rename = require( 'gulp-rename' ),
    del = require( 'del' ),
    less = require( 'gulp-less' ),
    cleanCSS = require( 'gulp-clean-css' ),
    concat = require( 'gulp-concat' ),
    uglify = require( 'gulp-uglify' ),
    autoprefixer = require( 'gulp-autoprefixer' ),
    merge = require( 'merge-stream' );

/* jshint undef: false */
function distCore() {
    return gulp.src( 'node_modules/livevalidator/js/*.js' )
        .pipe( concat( 'livevalidator.js' ) )
        .pipe( gulp.dest( 'static/dist/js' ) )
        .pipe( uglify() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'static/dist/js' ) );
}

function distPluginJQuery() {
    return gulp.src( [
        'node_modules/livevalidator/js/*.js',
        'node_modules/livevalidator-plugin-jquery/js/*.js'
    ] )
        .pipe( concat( 'livevalidator-jquery.js' ) )
        .pipe( gulp.dest( 'static/dist/js' ) )
        .pipe( uglify() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'static/dist/js' ) );
}

function distThemeDefaultCSS() {
    return gulp.src( 'node_modules/livevalidator-theme-default/less/Default.less' )
        .pipe( less() )
        .pipe( autoprefixer( {
            browsers: [ 'last 2 versions', '> 1%' ]
        } ) )
        .pipe( gulp.dest( 'static/dist/css/themes' ) )
        .pipe( cleanCSS() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'static/dist/css/themes' ) );
}

function distThemeDefaultJS() {
    return gulp.src( 'node_modules/livevalidator-theme-default/js/Default.js' )
        .pipe( gulp.dest( 'static/dist/js/themes' ) )
        .pipe( uglify() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'static/dist/js/themes' ) );
}

function distThemeBootstrap3JS() {
    var bs3 = gulp.src( 'node_modules/livevalidator-theme-bootstrap3/js/Bootstrap3.js' );

    var bs3Popover = gulp.src( [
        'node_modules/livevalidator-theme-bootstrap3/js/Bootstrap3.js',
        'node_modules/livevalidator-theme-bootstrap3/js/Bootstrap3Popover.js'
    ] )
        .pipe( concat( 'Bootstrap3Popover.js' ) );

    var bs3Tooltip = gulp.src( [
        'node_modules/livevalidator-theme-bootstrap3/js/Bootstrap3.js',
        'node_modules/livevalidator-theme-bootstrap3/js/Bootstrap3Tooltip.js'
    ] )
        .pipe( concat( 'Bootstrap3Tooltip.js' ) );

    return merge( bs3, bs3Popover, bs3Tooltip )
        .pipe( gulp.dest( 'static/dist/js/themes' ) )
        .pipe( uglify() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'static/dist/js/themes' ) );
}

function previewThemeBootstrap3() {
    var styles = gulp.src( [
        'bower_components/bootstrap/less/mixins/*.less',
        'bower_components/bootstrap/less/variables.less',
        'bower_components/bootstrap/less/forms.less',
        'bower_components/bootstrap/less/buttons.less',
        'bower_components/bootstrap/less/type.less',
        'bower_components/bootstrap/less/tooltip.less',
        'bower_components/bootstrap/less/popovers.less'
    ] )
        .pipe( concat( 'Bootstrap3' ) )
        .pipe( less() )
        .pipe( autoprefixer( {
            browsers: [ 'last 2 versions', '> 1%' ]
        } ) )
        .pipe( cleanCSS() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'static/preview/themes' ) );

    var js = gulp.src( [
        'bower_components/bootstrap/js/tooltip.js',
        'bower_components/bootstrap/js/popover.js'
    ] )
        .pipe( concat( 'Bootstrap3.js' ) )
        .pipe( uglify() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'static/preview/themes' ) );

    return merge( styles, js );
}

function distThemeUIkit2JS() {
    var uikit2 = gulp.src( 'node_modules/livevalidator-theme-uikit2/js/UIkit2.js' );

    var uikit2Tooltip = gulp.src( [
        'node_modules/livevalidator-theme-uikit2/js/UIkit2.js',
        'node_modules/livevalidator-theme-uikit2/js/UIkit2Tooltip.js'
    ] )
        .pipe( concat( 'UIkit2Tooltip.js' ) );

    return merge( uikit2, uikit2Tooltip )
        .pipe( gulp.dest( 'static/dist/js/themes' ) )
        .pipe( uglify() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'static/dist/js/themes' ) );
}

function previewThemeUIkit2() {
    var styles = gulp.src( [
        'bower_components/uikit/src/less/uikit-variables.less',
        'bower_components/uikit/src/less/components/tooltip.less'
    ] )
        .pipe( concat( 'UIkit2' ) )
        .pipe( less() )
        .pipe( autoprefixer( {
            browsers: [ 'last 2 versions', '> 1%' ]
        } ) )
        .pipe( cleanCSS() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'static/preview/themes' ) );

    var js = gulp.src( [
        'bower_components/uikit/src/js/components/tooltip.js'
    ] )
        .pipe( concat( 'UIkit2.js' ) )
        .pipe( uglify() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'static/preview/themes' ) );

    return merge( styles, js );
}

function distTranslations() {
    return gulp.src( 'node_modules/livevalidator-translation-*/js/*.js' )
        .pipe( rename( { dirname: '' } ) )
        .pipe( gulp.dest( 'static/dist/js/translations' ) )
        .pipe( uglify() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'static/dist/js/translations' ) );
}

function distTesterHtml5() {
    return gulp.src( 'node_modules/livevalidator-tester-html5/js/*.js' )
        .pipe( concat( 'html5.js' ) )
        .pipe( gulp.dest( 'static/dist/js/tester' ) )
        .pipe( uglify() )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( gulp.dest( 'static/dist/js/tester' ) );
}

function clean( cb ) {
    del( [ 'static/dist', 'static/preview' ] ).then( function() { cb(); } );
}

function dist() {
    return merge(
        distCore(),

        distPluginJQuery(),

        distThemeDefaultCSS(),
        distThemeDefaultJS(),
        distThemeBootstrap3JS(),
        distThemeUIkit2JS(),

        distTesterHtml5(),

        distTranslations()
    );
}

function previews() {
    return merge(
        previewThemeBootstrap3(),
        previewThemeUIkit2()
    );
}

gulp.task( 'code-standards', function() {
    return gulp.src( [ '*.js' ] )
            .pipe( jshint() )
            .pipe( jscs( { configPath: '.jscsrc' } ) )
            .pipe( stylish.combineWithHintResults() )
            .pipe( jshint.reporter( 'jshint-stylish' ) )
            .pipe( jscs.reporter( 'fail' ) )
            .pipe( jshint.reporter( 'fail' ) );
} );

gulp.task( 'dist-core', distCore );

gulp.task( 'dist-plugin-jquery', distPluginJQuery );

gulp.task( 'dist-theme-default-css', distThemeDefaultCSS );
gulp.task( 'dist-theme-default-js', distThemeDefaultJS );
gulp.task( 'dist-theme-default', [ 'dist-theme-default-css', 'dist-theme-default-js' ] );

gulp.task( 'dist-theme-bootstrap3', distThemeBootstrap3JS );

gulp.task( 'dist-theme-uikit2', distThemeUIkit2JS );

gulp.task( 'dist-js-translations', distTranslations );

gulp.task( 'dist-tester-html5', distTesterHtml5 );

gulp.task( 'clean', clean );

gulp.task( 'dist', dist );

gulp.task( 'previews', previews );

gulp.task( 'default', [ 'clean' ], function() {
    return merge(
        dist(),
        previews()
    );
} );
