var gulp = require( 'gulp' ),
    less = require( 'gulp-less' ),
    concat = require( 'gulp-concat' ),
    rename = require( 'gulp-rename' ),
    del = require( 'del' ),
    uglify = require( 'gulp-uglify' ),
    cleanCSS = require( 'gulp-clean-css' ),
    autoprefixer = require( 'gulp-autoprefixer' )
    merge = require( 'merge-stream' );

var uikitLess = 'assets/less/uikit.less';
var uikitJS =  [
            'assets/bower_components/uikit/src/js/core/core.js',
            'assets/bower_components/uikit/src/js/core/dropdown.js',
            'assets/bower_components/uikit/src/js/core/offcanvas.js',
            'assets/bower_components/uikit/src/js/core/tab.js',
            'assets/bower_components/uikit/src/js/core/scrollspy.js',
            'assets/bower_components/uikit/src/js/core/smooth-scroll.js',
            'assets/bower_components/uikit/src/js/core/switcher.js',
            'assets/bower_components/uikit/src/js/core/utility.js',
            'assets/bower_components/uikit/src/js/components/sticky.js'
        ];
var prismJS =  [
            'assets/bower_components/prism/components/prism-core.js',
            'assets/bower_components/prism/components/prism-markup.js',
            'assets/bower_components/prism/components/prism-clike.js',
            'assets/bower_components/prism/components/prism-javascript.js',
            'assets/bower_components/prism/components/prism-css.js'
        ];

function cssUikit() {
    return gulp.src( uikitLess )
            .pipe( less( {
                paths: './assets/bower_components/uikit/src/less'
            } ) )
            .pipe( autoprefixer( {
                browsers: [ 'last 2 versions', '> 1%' ]
            } ) )
            .pipe( cleanCSS() )
            .pipe( rename( { suffix: '.min' } ) )
            .pipe( gulp.dest( 'static/css' ) );
}

function fontsUikit() {
    return gulp.src( 'assets/bower_components/uikit/src/fonts/*' )
            .pipe( gulp.dest( 'static/fonts' ) );
}

function jsUikit() {
    return gulp.src( uikitJS )
            .pipe( uglify() )
            .pipe( concat( 'uikit.min.js' ) )
            .pipe( gulp.dest( 'static/js' ) );
}

function cssPrism() {
    return gulp.src( [ 'assets/bower_components/prism/themes/prism.css' ] )
            .pipe( cleanCSS() )
            .pipe( rename( { suffix: '.min' } ) )
            .pipe( gulp.dest( 'static/css' ) );
}

function jsPrism() {
    return gulp.src( prismJS )
            .pipe( uglify() )
            .pipe( concat( 'prism.min.js' ) )
            .pipe( gulp.dest( 'static/js' ) );

}

function clean( cb ) {
    del( 'static/*' ).then( function() { cb(); } );
}

gulp.task( 'css-uikit', cssUikit );
gulp.task( 'fonts-uikit', fontsUikit );
gulp.task( 'js-uikit', jsUikit );

gulp.task( 'css-prism', cssPrism );
gulp.task( 'js-prism', jsPrism );

gulp.task( 'clean', clean );

gulp.task( 'default', [ 'clean' ], function() {
    return merge(
        cssUikit(),
        jsUikit(),
        fontsUikit(),

        cssPrism(),
        jsPrism()
    );
} );

gulp.task( 'watch', function() {
    gulp.watch( [ 'assets/less/uikit.less', 'assets/less/theme.less', 'assets/less/uikit/*.less' ], [ 'css-uikit' ] );
} );
